#!/usr/bin/env python
# Author: Craig
# not exactly straightforward because of prepended origsize and iv
# and appended padding

import os
import sys
import struct
import subprocess
import filecmp
from Crypto import Random

sys.path.append('../src')
from aes_wrapper import encrypt_file


filename = sys.argv[1]

# encrypt suing PyCrypto
key = Random.get_random_bytes(16)
enc_filename = encrypt_file(key, filename)

# decrypt using openssl
f = open(enc_filename, 'rb')
origsize = struct.unpack('<Q', f.read(struct.calcsize('Q')))[0]
iv = f.read(16)
padded = f.read()
f.close()

norm_filename = filename + '.norm'
g = open(norm_filename, 'wb')
g.write(padded)
g.close()

dec_filename = filename + '.dec'
cmd = 'openssl enc -d -aes-128-cbc -K %s -iv %s -in %s -out %s -nopad' \
      % (key.encode('hex'), iv.encode('hex'), norm_filename, dec_filename)
# print 'cmd: ', cmd
subprocess.call(cmd.split())

out_filename = filename + '.out'
f = open(dec_filename, 'rb')
g = open(out_filename, 'wb')
g.write(f.read())
g.truncate(origsize)
f.close()
g.close()

print filecmp.cmp(filename, out_filename)

# cleanup
prefix = os.path.splitext(filename)[0]
cmd_prefix = 'rm %s' % prefix
os.system(cmd_prefix + '.enc')
os.system(cmd_prefix + '.norm')
os.system(cmd_prefix + '.dec')
os.system(cmd_prefix + '.out')

