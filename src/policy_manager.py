#!/usr/bin/env python
# Author: Trevor

import os
import sys
from jsonrpclib.SimpleJSONRPCServer import SimpleJSONRPCServer


datasets = { 'COI_1':{'A':[],'B':[]}, 'COI2':{'C':[], 'D':[]} }
history = {}


def handle_policy_1(username, full_filename, COI, Company):
    filename = os.path.split(full_filename)[1]

    print username, filename, COI, Company

    if not history.has_key(username):
        history[username] = []

    # check if file already in system
    if filename in datasets[COI][Company]:
        for coi,cmpny in history[username]:
            if (coi == COI) and (cmpny == Compnay):
                return 1
            else:
                return 0

    datasets[COI][Company].append(filename)


    for coi,cmpny in history[username]:
        if (coi == COI) and (cmpny != Company):
            return 0

    history[username].append( (COI,Company) )
    print history
    return 1




def main(addr):
    server = SimpleJSONRPCServer((addr, 9090))
    server.register_function(handle_policy_1)
    server.serve_forever()

if __name__ == '__main__':
    main(sys.argv[1])