#!/usr/bin/env python
# Author: Trevor

import sys
import jsonrpclib


server = jsonrpclib.Server('http://10.8.207.248:9090')
server_handle_policy = getattr(server, 'handle_policy_%s' % sys.argv[1])
status = server_handle_policy(sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
print >> sys.stdout, status

