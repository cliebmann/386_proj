#!/usr/bin/env python
# Author: Craig

from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer

import sys


RECV_DIR = '/home/cliebman/386/recv_dir'

def main(ip):
    authorizer = DummyAuthorizer()
    authorizer.add_anonymous(RECV_DIR, perm=('r','w'))

    handler = FTPHandler
    handler.authorizer = authorizer

    address = (ip, 2121)
    server = FTPServer(address, handler)

    server.serve_forever()

if __name__ == '__main__':
    main(sys.argv[1])