#!/usr/bin/env python
# Author: Kuni

import gnupg
import pickle
import sys
from jsonrpclib.SimpleJSONRPCServer import SimpleJSONRPCServer

DM1_FP = u'D30CDC8C72E7DEE32D0BFDE05BFDFAB2D11B5FFF'

gpg = gnupg.GPG(gnupghome='/Users/craig/.gnupg')

DOMAINS_DIR = '/Users/craig/Documents/ELE386/Assignments/Project/domains/'

DEBUG = True

def log(s):
    if DEBUG:
        print >> sys.stderr, 'log:', s

def register_key(hwAddr, key, domain):
    global gpg
    global DOMAINS_DIR
    global DM1_FP

    # return (0,1) or maybe more descriptive AND certificate
    # should certificate be sent separately? FTP?
    filename = DOMAINS_DIR + domain + ".pk"
    try:
        f = open(filename, 'rb')
    except:
        log('could not open domain file')
        return (0, '')
    keys = pickle.load(f)
    f.close()

    # check 'domain' is a valid domain

    #check to see if it is already there
    if keys.has_key(hwAddr):
        (status, fps) = keys[hwAddr]
        if status == 0: #previously revoked
            return (-1, '')
        elif status == 1: #already there
            return (0, '')

    #unregistered, so we need to add it 
    importresult = gpg.import_keys(key)
    keys[hwAddr] = (1, importresult.fingerprints)

    f = open(filename, 'wb')
    pickle.dump(keys, f)
    f.close()

    #certificate stuff
    f = open('cert','wb')
    print >> f, domain  # domain manager
    print >> f, hwAddr    # sender
    print >> f, key
    f.close()
    
    cert_file = open('cert','rb')
    sig = gpg.sign_file(cert_file, detach=True, keyid=DM1_FP, passphrase='ele386')
    cert_file.close()

    cert_file = open('cert', 'rb')
    my_cert = cert_file.read()
    my_cert_sig = sig.data
    cert_file.close()

    return (1, (my_cert, my_cert_sig))


def process_symmetric_key(encrypted_key, requestHwAddr, requestDomain):
    global gpg
    global DOMAINS_DIR

    # encrypt w requesting device's public key
    filename = DOMAINS_DIR + requestDomain + '.pk'
    f = open(filename, 'rb')
    keys = pickle.load(f)

    if not keys.has_key(requestHwAddr):
        return 0

    (status, recipient) = keys[requestHwAddr]

    if status == 1:
        # decrypt w DM's private key
        #f = open('tmp.key','wb')
        #f.write(encrypted_key)
        #f.close()

        #f = open('tmp.key','rb')

        decrypted_key = gpg.decrypt(encrypted_key, passphrase='ele386')
        log('process: ok? ', decrypted_key.ok)
        symmetric_key = gpg.encrypt(decrypted_key.data, recipient[0], always_trust=True)
        log('process: ok?', symmetric_key.ok)
        return symmetric_key.data

    return 0

def main():
    server = SimpleJSONRPCServer(('10.9.211.152', 8080))
    server.register_function(register_key)
    server.register_function(process_symmetric_key)
    server.serve_forever()


def revoke(hwAddr, domain):
    #return 1 on success 0 on failure
    if not keys.has_key(hwAddr):
        return 0
    filename = DOMAINS_DIR + domain + ".pk"
    f = open(filename, 'rb')
    keys = pickle.load(f)
    f.close()

    keys[hwAddr][0] = -1
    f = open(filename, 'wb')
    pickle.dump(keys, f)
    f.close()
    return 1

def unrevoke(hwAddr, domain):
    #return 1 on success 0 on failure
    if not keys.has_key(hwAddr):
        return 0
    filename = DOMAINS_DIR + domain + ".pk"
    f = open(filename, 'rb')
    keys = pickle.load(f)
    f.close()

    keys[hwAddr][0] =  1
    f = open(filename, 'wb')
    pickle.dump(keys, f)
    f.close()
    return 1

if __name__ == '__main__':
    if sys.argv[1] == 'serv':
        main()
    elif sys.argv[1] == 'revoke':
        revoke(sys.argv[2], sys.argv[3])
    elif sys.argv[1] == 'unrevoke':
        unrevoke(sys.argv[2], sys.argv[3])

