#!/usr/bin/env python
# Author: Craig

import os, sys
from Crypto import Random
from aes_wrapper import encrypt_file, decrypt_file
import gnupg
import tarfile
import jsonrpclib
import pickle
import getpass
import subprocess

import traceback
from ftplib import FTP

from messenger_utils import login, get_hw_addr, 
                            user_to_hw_addr, hw_to_ip_addr


# Fingerprints
DM1_FP = u'D30CDC8C72E7DEE32D0BFDE05BFDFAB2D11B5FFF'

TMP_DIR = '/home/cliebman/386/tmp_dir/'
RECV_DIR = '/home/cliebman/386/recv_dir/'
SEND_DIR = '/home/cliebman/386/send_dir/'


DEBUG = False

def log(s):
    if DEBUG:
        print >> sys.stderr, 'log:', s



def send(send_filename, domain_no, policy_filename,  dstIP):
    gpg = gnupg.GPG(gnupghome='/home/cliebman/.gnupg')

    # authenticate user
    user = raw_input("Username: ")
    authentication_status = login(user)
    if not authentication_status:
        sys.exit("Incorrect Credentials")

    key = Random.get_random_bytes(16)

    name = os.path.split(send_filename)[1]
    outfilename=name+'.enc'

    enc_data_filename = encrypt_file(key, send_filename, out_filename=outfilename)

    encrypted_key = gpg.encrypt(key, DM1_FP, always_trust=True)

    enc_key_filename = 'encrypted_key'
    enc_key_file = open(enc_key_filename, 'wb')
    enc_key_file.write(encrypted_key.data)
    enc_key_file.close()

    meta_filename = 'meta'
    meta_file = open(meta_filename, 'wb')
    meta_file.write('%d' % domain_no)
    meta_file.close()


    # print 'inner tar:'
    # print meta_filename
    # print enc_key_filename
    # print enc_data_filename
    # print policy_filename


    tar_filename = 'mprime.tar'
    tar = tarfile.open(tar_filename, 'w')
    tar.add(meta_filename)
    tar.add(enc_key_filename)
    tar.add(enc_data_filename)
    tar.add(policy_filename)
    tar.close()

    tar_file = open(tar_filename,'r')
    pphrase = getpass.getpass('Passphrase: ')
    orig_sig = gpg.sign_file(tar_file, detach=True, keyid=get_hw_addr('eth1'), passphrase=pphrase)
    tar_file.close()
    orig_sig_filename = 'mprime.tar.sig'
    orig_sig_file = open(orig_sig_filename, 'wb')
    orig_sig_file.write(orig_sig.data)
    orig_sig_file.close()

    cert_filename = 'cert' # 
    cert_sig_filename = 'cert.sig'

    # print 'outer tar:'
    # print tar_filename
    # print orig_sig_filename
    # print cert_filename
    # print cert_sig_filename

    message_tar_filename = SEND_DIR + name + '.tar'
    message_tar = tarfile.open(message_tar_filename, 'w')
    message_tar.add(tar_filename)
    message_tar.add(orig_sig_filename)
    message_tar.add(cert_filename)
    message_tar.add(cert_sig_filename)
    message_tar.close()

    # print message_tar_filename


    ftp = FTP()
    # print ipAddr
    ftp.connect(ipAddr, port=2121)

    try:
        try:
            print 'Sending file...'
            ftp.login()
            name = os.path.split(message_tar_filename)[1]
            f = open(message_tar_filename)
            ftp.storbinary('STOR ' + name, f)
            f.close()
            print 'Successful'
        finally:
            print 'Quitting...'
            ftp.quit()
    except:
        traceback.print_exc()

    # cleanup files???
    os.system('rm %s' % enc_data_filename)
    os.system('rm encrypted_key')
    os.system('rm meta')
    os.system('rm mprime.tar')
    os.system('rm mprime.tar.sig')


def recv(recv_filename):
    gpg = gnupg.GPG(gnupghome='/home/cliebman/.gnupg')

    # authenticate user
    user = raw_input("Username: ")
    authentication_status = login(user)
    if not authentication_status:
        sys.exit("Incorrect Credentials")

    tar = tarfile.open(recv_filename, 'r')
    tar.extractall(TMP_DIR)
    # creates my_cert, my_cert.sig, mprime.tar, mprime.tar.sig
    tar.close()

    tar = tarfile.open(TMP_DIR + 'mprime.tar')
    tar.extractall(TMP_DIR)
    # creates meta, encrypted_key, 'filename'.enc, policy
    tar.close()


    full_filename = os.path.split(recv_filename)[1]
    filename = os.path.splitext(full_filename)[0]


    # get domain from meta
    meta_file = open(TMP_DIR + 'meta', 'rb')
    domain = meta_file.readline()
    # anything else?
    meta_file.close()


    # check policy
    # give filename, username, policy?

    for f in os.listdir(TMP_DIR):
        if f.startswith('policy'):
            policy_filename = f
    f = open(TMP_DIR+policy_filename,'rb')
    policy_name = f.readline()
    coi = f.readline()
    company = f.readline()
    f.close()

    cmd = "python ./policy_handler.py %s %s %s %s %s" % (domain, user, filename, coi.rstrip(), company.rstrip())
    proc = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
    trevor_status = True if proc.stdout.readline() == '1\n' else False

    #trevor_status=True

    if not trevor_status:
        print 'you are not authorized to read this file'
        return -1



    # verify certificate given as two files, C and Cert
    cert_filename = TMP_DIR + 'cert'
    cert_sig_filename = TMP_DIR + 'cert.sig'
    cert_sig_stream = open(cert_sig_filename, 'rb')
    verified = gpg.verify_file(cert_sig_stream, cert_filename)
    if not verified:
        print 'certificate could not be verified'
        return -1
    else:
        log('recv -- certificate verified')


    # trust certificate, so get sender's pk
    cert_file = open(TMP_DIR + 'cert', 'rb')
    cert_file.readline() # domain manager ... ?
    cert_file.readline() # sender hwAddr
    sender_PK = cert_file.read()
    cert_file.close()
    log(sender_PK)
    import_result = gpg.import_keys(sender_PK)


    # verify mprime
    mp_filename = TMP_DIR + 'mprime.tar'
    mp_sig_filename = TMP_DIR + 'mprime.tar.sig'
    mp_sig_stream = open(mp_sig_filename, 'rb')
    verified = gpg.verify_file(mp_sig_stream, mp_filename)
    if not verified:
        print 'mprime could not be verified'
        return 0
    else:
        log('recv -- mprime verified')


    


    # ask DM for Symmetric Key
    encrypted_key = open(TMP_DIR + 'encrypted_key', 'rb').read()
    hwAddr = get_hw_addr('eth1')

    log(encrypted_key)
    log(hwAddr)
    log(domain)

    server = jsonrpclib.Server('http://10.9.211.152:8080')
    reencrypted_key = server.process_symmetric_key(encrypted_key, hwAddr, int(domain))

     # check for errors
    if reencrypted_key == 0:
        print 'Domain Manager could not verify this machine'
        return 0

    pphrase = getpass.getpass('Passphrase: ')
    decrypted_key = gpg.decrypt(reencrypted_key, passphrase=pphrase)

    # decrypt_data
    encrypted_file =  TMP_DIR + filename + '.enc'
    outfilename = RECV_DIR + filename
    plaintext = decrypt_file(decrypted_key.data, encrypted_file, out_filename=outfilename)

    print 'decrypted file: %s' % outfilename

    return 1


if __name__ == '__main__':
    if sys.argv[1] == 'send':
        # sys.argv[2] == filename
        # sys.argv[3] == domain number
        # sys.argv[4] == policy_file/number?
        if sys.argv[5] == '-u':
            hwAddr = user_to_hw_addr(sys.argv[6])
            ipAddr = hw_to_ip_addr(hwAddr)
        elif sys.argv[5] == '-h':
            ipAddr = hw_to_ip_addr(sys.argv[6])
        else:
            ipAddr = sys.argv[5]
        print ipAddr
        send(sys.argv[2], int(sys.argv[3]), sys.argv[4], ipAddr)

    if sys.argv[1] == 'recv':
        recv(sys.argv[2])

        # cleanup
        cmd = 'rm -rf %s*' % TMP_DIR
        os.system(cmd)



