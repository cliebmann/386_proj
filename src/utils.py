# Author: Craig

import fcntl, socket, struct
import getpass
import hashlib


USERS_DIR = '/home/cliebman/386/users_dir/'

HW_TO_IP = '/home/cliebman/386/HW_TO_IP.pk'
USER_TO_HW = '/home/cliebman/386/USER_TO_HW.pk'


def get_hw_addr(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    info = fcntl.ioctl(s.fileno(), 0x8927, struct.pack('256s', ifname[:15]))
    return ''.join(['%02x:' % ord(char) for char in info[18:24]])[:-1]


def login(user):
    try:
        user_f = open(USERS_DIR+user, 'rw')
    except IOError:
        sys.exit("User '%s' does not exist" % user)
    saved_pword_hash = user_f.readline().strip()
    # check that user matches
    pword_hash = 0
    i = 0
    while pword_hash != saved_pword_hash:
        pword = getpass.getpass()
        m = hashlib.sha256()
        m.update(pword)
        pword_hash = m.digest()
        i += 1
        if i == 3:
            break
    user_f.close()
    if i == 3:
        return 0

    return 1 


def user_to_hw_addr(user):
    f = open(USER_TO_HW, 'rb')
    d = pickle.load(f)
    f.close()

    if d.has_key(user):
        return d[user]
    else:
        return None

def hw_to_ip_addr(hwAddr):
    f = open(HW_TO_IP, 'rb')
    d = pickle.load(f)
    f.close()

    if d.has_key(hwAddr):
        return d[hwAddr]
    else:
        return None

def set_user_to_hw_addr(user, hwAddr):
    f = open(USER_TO_HW, 'rb')
    d = pickle.load(f)
    f.close()

    d[user] = hwAddr

    f = open(USER_TO_HW, 'wb')
    pickle.dump(d, f)
    f.close()

def set_hw_to_ip_addr(hwAddr, ipAddr):
    f = open(HW_TO_IP, 'rb')
    d = pickle.load(f)
    f.close()

    d[hwAddr] = ipAddr

    f = open(HW_TO_IP, 'wb')
    pickle.dump(d, f)
    f.close()



