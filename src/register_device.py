#!/usr/bin/env python
# Author: Kuni

import os, sys
import fcntl, socket, struct
import gnupg
import jsonrpclib
import getpass

# Fingerprints
DM1_FP = 'D30C DC8C 72E7 DEE3 2D0B  FDE0 5BFD FAB2 D11B 5FFF'


DEBUG = True

def log(s):
    if DEBUG:
        print >> sys.stderr, 'log:', s

def get_hw_addr(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    info = fcntl.ioctl(s.fileno(), 0x8927, struct.pack('256s', ifname[:15]))
    return ''.join(['%02x:' % ord(char) for char in info[18:24]])[:-1]


def register():
    ifname = raw_input('Interface: ')
    domain = raw_input('Domain: ')
    email  = raw_input('Email: ')

    hwAddr = get_hw_addr(ifname)

    gpg = gnupg.GPG(gnupghome='/home/cliebman/.gnupg')

    key = gpg.export_keys(hwAddr)

    if not len(key):
        print 'creating new key'
        pp = getpass.getpass('Passphrase: ')
        input_data = gpg.gen_key_input(key_type="RSA", key_length=1024,
                                   name_real=hwAddr,
                                   name_email=email,
                                   passphrase=pp)

        key = gpg.gen_key(input_data)
        key = gpg.export_keys(hwAddr)
    else:
        log('already made key for this interface!')


    server = jsonrpclib.Server('http://10.9.211.152:8080')
    (status, certficates) = server.register_key(hwAddr, key, domain)
    
    if status == 1:
        (my_cert, my_cert_sig) = certficates
        f = open('cert', 'wb')
        f.write(my_cert)
        f.close()
        
        f = open('cert.sig', 'wb')
        f.write(my_cert_sig)
        f.close()
    elif status == 0:
        print 'already registered'
    else:
        print 'registeration failed: status code ' + status
    
    

if __name__ == '__main__':
    register()